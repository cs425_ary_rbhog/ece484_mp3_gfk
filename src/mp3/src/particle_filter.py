import numpy as np
from maze import Maze, Particle, Robot
import bisect
import rospy
from gazebo_msgs.msg import  ModelState
from gazebo_msgs.srv import GetModelState
import shutil
from std_msgs.msg import Float32MultiArray
from scipy.integrate import ode
import turtle
import random
import matplotlib.pyplot as plt

def vehicle_dynamics(t, vars, vr, delta):
    curr_x = vars[0]
    curr_y = vars[1] 
    curr_theta = vars[2]
    
    dx = vr * np.cos(curr_theta)
    dy = vr * np.sin(curr_theta)
    dtheta = delta

    return [dx,dy,dtheta]

class particleFilter:
    def __init__(self, bob, world, num_particles, sensor_limit, x_start, y_start):
        self.num_particles = num_particles  # The number of particles for the particle filter
        self.sensor_limit = sensor_limit    # The sensor limit of the sensor
        particles = list()

        ##### TODO:  #####
        # Modify the initial particle distribution to be within the top-right quadrant of the world, and compare the performance with the whole map distribution.
        for i in range(num_particles):

            # (Default) The whole map
            x = np.random.uniform(0, world.width)
            y = np.random.uniform(0, world.height)

            # x = np.random.uniform(0, world.width / 2)
            # y = np.random.uniform(0, world.height / 2)
            # # first quadrant
            # x += 60
            # y += 75/2

            # particles.append(Particle(x = bob.x, y = bob.y, heading = bob.heading, maze = world, sensor_limit = sensor_limit, noisy=True))
            particles.append(Particle(x = x, y = y, maze = world, sensor_limit = sensor_limit, noisy=True))
        ###############

        self.particles = particles          # Randomly assign particles at the begining
        self.bob = bob                      # The estimated robot state
        self.world = world                  # The map of the maze
        self.x_start = x_start              # The starting position of the map in the gazebo simulator
        self.y_start = y_start              # The starting position of the map in the gazebo simulator
        self.modelStatePub = rospy.Publisher("/gazebo/set_model_state", ModelState, queue_size=1)
        self.controlSub = rospy.Subscriber("/gem/control", Float32MultiArray, self.__controlHandler, queue_size = 1)
        self.control = []                   # A list of control signal from the vehicle

        # self.weights = [1 / num_particles] * num_particles

        # self.r.set_initial_value(particle.state, 0)
        return

    def __controlHandler(self,data):
        """
        Description:
            Subscriber callback for /gem/control. Store control input from gem controller to be used in particleMotionModel.
        """
        tmp = list(data.data)
        self.control.append(tmp)

    def getModelState(self):
        """
        Description:
            Requests the current state of the polaris model when called
        Returns:
            modelState: contains the current model state of the polaris vehicle in gazebo
        """

        rospy.wait_for_service('/gazebo/get_model_state')
        try:
            serviceResponse = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            modelState = serviceResponse(model_name='polaris')
        except rospy.ServiceException as exc:
            rospy.loginfo("Service did not process request: "+str(exc))
        return modelState

    def weight_gaussian_kernel(self,x1, x2, std = 5000):
        if x1 is None: # If the robot recieved no sensor measurement, the weights are in uniform distribution.
            return 1./len(self.particles)
        else:
            tmp1 = np.array(x1)
            tmp2 = np.array(x2)
            return np.sum(np.exp(-((tmp2-tmp1) ** 2) / (2 * std)))


    def updateWeight(self, readings_robot):
        """
        Description:
            Update the weight of each particles according to the sensor reading from the robot 
        Input:
            readings_robot: List, contains the distance between robot and wall in [front, right, rear, left] direction.
        """

        ## TODO #####
        weights_new = list()
        if readings_robot is not None:          
            for particle in self.particles:
                # print(readings_robot, particle.read_sensor())
                weight = self.weight_gaussian_kernel(readings_robot, particle.read_sensor())
                weights_new.append(weight)
        else:
            for particle in self.particles:
                weight = self.weight_gaussian_kernel(readings_robot, particle.read_sensor())
                weights_new.append(weight)

        weights_new = list(np.array(weights_new) / np.sum(np.array(weights_new)))

        for i in range(len(weights_new)):
            self.particles[i].weight = weights_new[i]
        
        # self.particles[0].x = self.bob.x
        # self.particles[0].y = self.bob.y
        # self.particles[0].heading = self.bob.heading


        # print(sum(self.weights))
        # self.weights = weights_new
        ###############
        # pass

    def resampleParticle(self):
        """
        Description:particle
            Perform resample to get a new list of particles 
        """
        particles_new = list()

        ## TODO #####
        weights = []
        for i in range(len(self.particles)):
            weights.append(self.particles[i].weight)
        cum_weights = np.cumsum(weights)

        # plt.clf()
        # plt.plot(cum_weights)
        # plt.pause(0.000000000000001)

        # plt.plot(cum_weights)
        # plt.show()
        for p in range(int(len(self.particles))):
            number = np.random.uniform()
            # very_noisy = np.random.uniform()
            # print(number)
            for i in range(len(cum_weights)):
                if number <= cum_weights[i]:
                    state = self.particles[i].state
                    new_particle = Particle(x=state[0], y=state[1], maze=self.world, weight=self.particles[i].weight, heading=state[2], sensor_limit=self.sensor_limit, noisy=True)
                    #     new_particle.add_noise()
                    # if very_noisy > 0.8:
                    #     new_particle = Particle(x=new_particle.add_noise(x=state[0], std=1), y=new_particle.add_noise(x=state[1], std=1), maze=self.world, weight=self.particles[i].weight, heading=state[2], sensor_limit=self.sensor_limit, noisy=True)
                    # else:
                    #     new_particle = Particle(x=state[0], y=state[1], maze=self.world, weight=self.particles[i].weight, heading=state[2], sensor_limit=self.sensor_limit, noisy=True)
                    #     new_particle.add_noise()
                    particles_new.append(new_particle)
                    # print(idx)
                    break
        # for p in range(int(len(self.particles)*0.1)):
            # x = np.random.uniform(0, self.world.width)
            # y = np.random.uniform(0, self.world.height)
            # new_particle = Particle(x=x, y=y, maze=self.world, sensor_limit=self.sensor_limit, noisy=True)
            # particles_new.append(new_particle)

        ###############

        self.particles = particles_new

    def particleMotionModel(self):
        """
        Description:
            Estimate the next state for each particle according to the control input from actual robot 
        """
        ## TODO #####
        # particles_new = list()

        for particle in self.particles:
            
            if len(self.control) != 0:
                r = ode(vehicle_dynamics)
                r.set_initial_value(particle.state)
                
                
                for control in self.control:
                    r.set_f_params(*control)
                    update = r.integrate(r.t + 0.01)
                    # print(r.t)
                # print(np.linalg.norm(np.array(particle.state) - np.array(update[:3])))
                # print(particle.state, update)
                    particle.x, particle.y, particle.heading = update
                # particles_new.append(Particle(x = update[0], y = update[1], maze = self.world, weight = particle.weight, heading = update[2], sensor_limit = self.sensor_limit))
        
        self.control = []
        # self.particles = particles_new
        ###############
        # pass


    def runFilter(self):
        """
        Description:
            Run PF localization
        """
        count = 0 
        fig, axs = plt.subplots(2)
        axs[1].set_title("Error in Heading")
        axs[1].set_ylabel("degrees (0)")
        axs[1].set_xlabel("iteration number")

        axs[0].set_title("Error in Euclidian Distance")
        axs[0].set_ylabel("meters (m)")
        while True:
            ## TODO: (i) Implement Section 3.2.2. (ii) Display robot and particles on map. (iii) Compute and save position/heading error to plot. #####
            self.world.clear_objects()
            self.particleMotionModel()
            reading = self.bob.read_sensor()

            self.updateWeight(reading)
            self.resampleParticle()

            self.world.show_robot(self.bob)
            
            # print(len(self.particles))
            self.world.show_particles(self.particles, show_frequency = 10)

            true_x, true_y, true_heading = self.bob.x, self.bob.y, (self.bob.heading*180/np.pi)%360
            est_x, est_y, est_heading = self.world.show_estimated_location(self.particles)

            error_dist = np.linalg.norm(np.array([true_x - est_x, true_y - est_y]))
            error_heading = np.linalg.norm(true_heading - (est_heading % 360))

            count += 1

            
            axs[0].scatter(count, error_dist, color="blue")
            axs[1].scatter(count, error_heading, color="green")

            plt.pause(0.0000000000000001)

            ###############
